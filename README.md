# To instal dependencies
npm i

# To start JSON server
npx json-server --watch data/db.json --port 8000

# To start client app
npm start