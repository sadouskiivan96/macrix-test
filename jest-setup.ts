/**
 * This file is used to set up jest with extra environmental helpers
 * so that we can write easier tests.
 */

/**
 * extends jest's `expect` to have more useful calls,
 * especially for use with @testing-library/react.
 * you can check them out here:
 * https://github.com/testing-library/jest-dom
 */

import '@testing-library/jest-dom';

