import type { FC, ReactElement } from 'react';
import { render, type RenderOptions } from '@testing-library/react';

import { BrowserRouter } from 'react-router-dom';
import React from 'react';

const renderWithAppContext: FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  return (
    <BrowserRouter>
      {children}
    </BrowserRouter>
  );
};

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, 'wrapper'>,
) => render(ui, { wrapper: renderWithAppContext, ...options });

export * from '@testing-library/react';

export { customRender as render };
