export interface User {
  firstName: string,
  lastName: string,
  street: string,
  aptNumber: number,
  postalCode: string,
  town: string,
  phone: string,
  dateOfBirthday: string,
  age: number,
  id: number
}