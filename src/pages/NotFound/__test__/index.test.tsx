import { fireEvent, screen } from '@testing-library/react';

import NotFound from '../index';
import { ROUTES } from '@/constants/routes';
import { render } from '../../../../utils/test.utils';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom') as any,
  useNavigate: () => mockedUsedNavigate,
}));
describe('NotFound', () => {
  it('renders NotFound component and shows text', () => {
    render(<NotFound />);
    expect(screen.getByText('Sorry, the page you visited does not exist.')).toBeInTheDocument();
  });

  it('should navigate to the users list when clicking the back to users button', () => {
    render(<NotFound />);
    fireEvent.click(screen.getByRole('button', { name: 'Back to users list' }));
    expect(mockedUsedNavigate).toHaveBeenCalledWith(ROUTES.USERS);
  });
});
