import { Button, Result } from "antd";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/routes";

function NotFound() {
  const navigate = useNavigate();
  const link = ROUTES.USERS;
  const backToUsers = () => {
    navigate(link);
  };
  return (
    <Result
      status="404"
      title="404"
      subTitle="Sorry, the page you visited does not exist."
      extra={<Button onClick={backToUsers} type="primary">Back to users list</Button>}
  />
  );
}

export default NotFound;
