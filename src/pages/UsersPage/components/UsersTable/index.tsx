/* eslint-disable jsx-a11y/anchor-is-valid */
import { Button, Popconfirm, Table } from "antd";
import EditableRow from "../EditableRow";
import EditableCell from "../EditableCell";
import { MouseEventHandler } from "react";
import './styles.scss';
import { UserDataSource } from "@/interfaces/user-data-sourse.interface";

const UsersTable:React.FC<{
  dataSource: UserDataSource[],
  handleDelete: Function,
  handleSaveRow: Function,
  handleAdd: MouseEventHandler<HTMLAnchorElement> & MouseEventHandler<HTMLButtonElement>,
  handleCancel: MouseEventHandler<HTMLAnchorElement> & MouseEventHandler<HTMLButtonElement>,
  handleSaveAll: MouseEventHandler<HTMLAnchorElement> & MouseEventHandler<HTMLButtonElement>,
}> = ({dataSource, handleDelete, handleSaveRow, handleAdd, handleCancel, handleSaveAll})=>{

  const defaultColumns = [
    {
      title: 'First name',
      dataIndex: 'firstName',
      editable: true,
      required: true
    },
    {
      title: 'Last name',
      dataIndex: 'lastName',
      editable: true,
      required: true
    },
    {
      title: 'Street',
      dataIndex: 'street',
      editable: true,
      required: true
    },
    {
      title: 'Apartment',
      dataIndex: 'aptNumber',
      editable: true,
      required: false
    },
    {
      title: 'Postal code',
      dataIndex: 'postalCode',
      editable: true,
      required: true
    },
    {
      title: 'Town',
      dataIndex: 'town',
      editable: true,
      required: true
    },
    {
      title: 'Phone',
      dataIndex: 'phone',
      editable: true,
      required: true
    },
    {
      title: 'Date of Birthday',
      dataIndex: 'dateOfBirthday',
      editable: true,
      required: true
    },
    {
      title: 'Age',
      dataIndex: 'age',
      required: true
    },
    {
      title: 'Action',
      dataIndex: 'operation',
      render: (_: any, record: UserDataSource) =>
      dataSource.length >= 1 ? (
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
            <a>Delete</a>
          </Popconfirm>
        ) : null,
    },
  ];

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };

  const columns = defaultColumns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record: UserDataSource) => ({
        record,
        editable: col.editable,
        required: col.required,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSaveRow
      }),
    };
  });

  return (
    <>
      <Button onClick={handleAdd} type="primary" style={{ marginBottom: 16 }}>
        Add a row
      </Button>
      <Table
        dataSource={dataSource}
        columns={columns}
        pagination={false}
        components={components}
        bordered
      />
      <div className="buttons-wrapper">
        <Button onClick={handleCancel} type="default" style={{ marginBottom: 16 }}>
          Cancel
        </Button>
        <Button onClick={handleSaveAll} type="primary" style={{ marginBottom: 16 }}>
          Save
        </Button>
      </div>
    </>
  )
}

export default UsersTable;