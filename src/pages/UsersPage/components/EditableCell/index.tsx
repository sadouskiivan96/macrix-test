import { useContext, useEffect, useRef, useState } from "react";
import EditableContext from "../EditableContext";
import { Form, Input } from "antd";
import './style.scss'
import { VALIDATION_MESSAGE } from "@/constants/validation-messages";

const EditableCell = ({
  title,
  editable,
  required,
  children,
  dataIndex,
  record,
  handleSaveRow,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef(null);
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing && inputRef.current) {
      inputRef.current.focus();
    }
  }, [editing]);
  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSaveRow({
        ...record,
        ...values,
        changedValueKey: Object.keys(values)[0]
      });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let rules: any = [
    {
      required: required,
      message: VALIDATION_MESSAGE.REQUIRED,
    }
  ];

  if(dataIndex === 'dateOfBirthday'){
    rules.push({
      pattern: /^\d{2}\/\d{2}\/\d{4}$/,
      message: VALIDATION_MESSAGE.DATE_FORMAT
    })
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={rules}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children && children[1] ? children : '-'}
      </div>
    );
  }
  return <td {...restProps}>{childNode}</td>;
};

export default EditableCell