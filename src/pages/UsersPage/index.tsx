import { useEffect, useState } from "react";
import UsersTable from "./components/UsersTable";
import './styles.scss';
import { UserDataSource } from "@/interfaces/user-data-sourse.interface";
import { User } from "@/interfaces/user.interface";
import { getUsers, saveUsers } from "@/services/user.service";

function UsersPage() {
  const [dataSource, setDataSource] = useState<UserDataSource[]>()
  useEffect(()=>{
    getUsersList()
  },[])

  const getUsersList = async () => {
    const data = await getUsers();

    let dataSource: UserDataSource[] = data.map((item: User)=>({
      ...item,
      key: item.id
    }))

    setDataSource(dataSource)
  }

  const handleSaveRow = (row) => {
    const newData = [...dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    if(row.changedValueKey === 'dateOfBirthday'){
      row.age = calculateAge(row.dateOfBirthday)
    }
    delete row.changedValueKey;
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    setDataSource(newData);
  };

  const calculateAge = (date: string) => {
    const dateParts = date.split("/");
    const day = parseInt(dateParts[0]);
    const month = parseInt(dateParts[1]);
    const year = parseInt(dateParts[2]);

    const today = new Date();
    const yearNow = today.getFullYear();
    const monthNow = today.getMonth() + 1;
    const dayNow = today.getDate();

    let age = yearNow - year;

    if (monthNow < month) {
      age--;
    }

    else if (monthNow === month && dayNow < day) {
      age--;
    }

    return age;
  }

  const handleAdd = () => {
    const date = new Date();
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();
    const formattedDate = `${day}/${month}/${year}`;

    const emptyUserData: UserDataSource = {
      key: dataSource.length + 1,
      id: dataSource.length + 1,
      firstName: "-",
      lastName: "-",
      street: "-",
      aptNumber: 0,
      postalCode: "-",
      town: "-",
      phone: "-",
      dateOfBirthday: formattedDate,
      age: 0
    };

    setDataSource([...dataSource, emptyUserData]);
  }

  const handleDelete = (key: number) => {
    const newData = dataSource.filter((item) => item.key !== key);
    setDataSource(newData);
  };

  const handleCancel = () => {
    getUsersList()
  }

  const handleSaveAll = async() => {
    let preparedUsersList: User[] = prepareUsersList()
    saveUsers(preparedUsersList)
  }

  const prepareUsersList = () => {
    return dataSource.map((data: UserDataSource): User =>{
      delete data.key;
      const newData = {
        ...data,
      }
      return newData;

    })
  }

  return (
    <div className="wrapper">
      <UsersTable
        dataSource={dataSource || []}
        handleDelete={handleDelete}
        handleSaveRow={handleSaveRow}
        handleAdd={handleAdd}
        handleSaveAll={handleSaveAll}
        handleCancel={handleCancel}
        />
    </div>
  );
}

export default UsersPage;
