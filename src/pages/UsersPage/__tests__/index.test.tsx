import { fireEvent, screen } from '@testing-library/react';
import { render } from '../../../../utils/test.utils';

import UsersPage from '../index';
import { getUsers, saveUsers } from '../../../services/user.service';

jest.mock('../../../services/user.service', () => ({
  getUsers: jest.fn(),
  saveUsers: jest.fn(),
}));

describe('UsersPage', () => {
  const mockData = [
    {
      id: 1,
      key: '1',
      firstName: 'John',
      lastName: 'Doe',
      dateOfBirthday: '-',
      age: 0,
      street: "-",
      aptNumber: 0,
      postalCode: "-",
      town: "-",
      phone: "-",
    },
  ];
  beforeEach(() => {
    (getUsers as jest.Mock).mockClear();
    (saveUsers as jest.Mock).mockClear();
  });


  it('should render UsersTable with data', async () => {
    (getUsers as jest.Mock).mockResolvedValue(mockData);

    render(<UsersPage />);

    await screen.findAllByText('John');

    expect(screen.getByText('John')).toBeInTheDocument();
  });

  it('should add a new user on handleAdd', async () => {
    (getUsers as jest.Mock).mockResolvedValue(mockData);

    render(<UsersPage />);

    await screen.findAllByText('John');
    const date = new Date();
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();
    const formattedDate = `${day}/${month}/${year}`;

    fireEvent.click(screen.getByText('Add a row'));

    expect(screen.getByText(formattedDate)).toBeInTheDocument();
  });

  it('should delete a user on handleDelete', async () => {
    (getUsers as jest.Mock).mockResolvedValue(mockData);

    render(<UsersPage />);

    await screen.findAllByText('John');

    fireEvent.click(screen.getByText('Delete'));

    fireEvent.click(screen.getByText('OK'));

    expect(screen.getByText('No data')).toBeInTheDocument();
  });

  it('should call saveUsers on handleSaveAll', async () => {
    (getUsers as jest.Mock).mockResolvedValue(mockData);

    render(<UsersPage />);
    await screen.findAllByText('John');

    fireEvent.click(screen.getByText('Save'));

    expect(saveUsers).toHaveBeenCalledTimes(1);

    expect(saveUsers).toHaveBeenCalledWith(mockData.map((item)=> {
      delete item.key;
      return item
    }));
  });
});