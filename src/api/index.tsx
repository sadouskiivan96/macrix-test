import axios, { InternalAxiosRequestConfig } from 'axios';
import { BASE_URL } from '../constants/env';

const api = axios.create({
  withCredentials: false,
  headers: {
    'Access-Control-Allow-Credentials': '*',
    'Access-Control-Allow-Origin': '*',
    'Content-Type':'application/json'
  },
  baseURL: `${BASE_URL}`,
});

api.interceptors.request.use((config: InternalAxiosRequestConfig) => config);

api.interceptors.response.use(
  (response) => response,
  (error) => Promise.reject(error?.response?.data),
);

export default api;
