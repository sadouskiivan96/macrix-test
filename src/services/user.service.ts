import { notification } from "antd";
import api from "../api";
import { User } from "@/interfaces/user.interface";

let id: number;

export function getUsers(): Promise<User[]>{
  let response = api.get('records').then((res)=>{
    const [data] = res.data
    id = data.id;
    return data.users
  })
  return response
}

export async function saveUsers(users: User[]): Promise<void>{
  try {
    await api.patch(`records/${id}`, {users});
    notification.success({
      message: "Success"
    })
  } catch(err){
    console.error('Error', err);
  }
}