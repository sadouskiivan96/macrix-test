export const VALIDATION_MESSAGE = {
  DATE_FORMAT : 'Please enter a valid date of birth in DD/MM/YYYY format.',
  REQUIRED: 'Field is required.',
}