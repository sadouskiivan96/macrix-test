import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { ROUTES } from './constants/routes';
import UsersPage from './pages/UsersPage';
import NotFound from './pages/NotFound';

export default function AppRoutes() {
  return (
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<Navigate to={ROUTES.USERS} />} />
          <Route path={ROUTES.USERS} element={<UsersPage />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
    </BrowserRouter>
  );
}
