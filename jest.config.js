module.exports = {
  collectCoverage: true,
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  preset: 'ts-jest',
  coverageReporters: ['html', 'text-summary', 'json-summary', 'cobertura'],
  collectCoverageFrom: [
    'src/**/*.{ts,tsx}',
    '!src/**/*.d.ts',
    '!**/node_modules/**',
  ],
  coverageDirectory: 'coverage',
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    '\\.(css|scss)$': '<rootDir>/scripts/jest/styleMock.js',
  },
  setupFiles: ['<rootDir>/scripts/jest/matchMedia.js'],
  setupFilesAfterEnv: ['<rootDir>/jest-setup.ts'],
};
